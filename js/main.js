import {createDot, createHorisLine, createVertLine} from './figures/figures.js'

const CELLSIZE = 50;

let levels = [
{
    config: {
        progress: 2,
        width: 5,
        height: 5
    },
    coordsMass: [
        {
            type: "vline",
            x: 1,
            y: 1.5
        },
        {
            type: "hline",
            x: 2.5,
            y: 3
        }
    ]
}, {
    config: {
        progress: 2,
        width: 7,
        height: 7
    },
    coordsMass: [
        {
            type: "point",
            x: 2,
            y: 2
        },
        {
            type: "point",
            x: 3,
            y: 3
        }   
    ]
}
];

creacteCanvas(levels, CELLSIZE, "myapp");

function creacteCanvas(levels, cell_size, id) {
    let levelNumber = 0;

    const CELLSIZE = cell_size;
    
    let currentLevelProgress = 0; 
    let coordsMass;
    let levelProgress;
    let fieldWidth;
    let fieldHeight;  
    
    let taskField;
    let playspace;
    let containerForAllLevel;

    changeMainValues(levelNumber);

    const canvas = document.getElementById(id);

    let app = new PIXI.Application({ 
        view: canvas,
        width: 1200,         // default: 800
        height: 600,        // default: 600
        antialias: true,    // default: false
        transparent: false, // default: false
        resolution: 1       // default: 1
    });

    app.renderer.backgroundColor = 0xF1E0B1;

    PIXI.loader
        .add("img/background.jpg")
        .load(startGame(app, CELLSIZE));    

    function setup() {
        containerForAllLevel = new PIXI.Container();
        containerForAllLevel.width = app.renderer.width;
        containerForAllLevel.height = app.renderer.height;

        playspace = new createField();

        taskField = new createField();
        taskField.x = app.renderer.width - taskField.width;
        
        drawLevel(taskField, coordsMass);

        let circle = new createDot(CELLSIZE);
        circle.x = CELLSIZE;
        circle.y = CELLSIZE;

        let line = new createHorisLine(CELLSIZE);
        line.x = CELLSIZE+0.5*CELLSIZE;
        line.y = CELLSIZE;

        containerForAllLevel.addChild(playspace);
        containerForAllLevel.addChild(taskField);
        containerForAllLevel.addChild(new createPanel());

        app.stage.addChild(containerForAllLevel);
    }

    function changeLevel() {
        levelNumber++;
        console.log("LEVEL NUMBER: ", levelNumber, levels.length);
        
        if (levelNumber < levels.length) {
            setTimeout(() => {
                app.stage.removeChild(containerForAllLevel);
                changeMainValues(levelNumber);
                setup(); 
            }, 1000);
        } else {
            setTimeout(() => {
                app.stage.removeChild(containerForAllLevel);
                changeMainValues(0);
                gameOverScene();
            }, 1000);
        }
    }

    function changeMainValues(levelNumber) {
        coordsMass = levels[levelNumber].coordsMass;
        currentLevelProgress = 0;
        levelProgress = levels[levelNumber].coordsMass.length;

        fieldWidth = levels[levelNumber].config.width * CELLSIZE;
        fieldHeight = levels[levelNumber].config.height * CELLSIZE;
    }

    function drawLevel(area, coordsMass) {
        coordsMass.forEach(element => {
            let type = element.type;    
            let x = element.x * CELLSIZE;
            let y = element.y * CELLSIZE;
            let figure;

            switch (type) {
                case "point":
                    figure = new createDot(CELLSIZE);
                    break;
                case "vline":
                    figure = new createVertLine(CELLSIZE);
                    break;
                case "hline":
                    figure = new createHorisLine(CELLSIZE);
                    break;
            
                default:
                    break;
            }
            
            figure.position.set(x, y);

            area.addChild(figure);
        });
    }

    function createField() {
        let container = new PIXI.Container();

        for (let i=0; i<fieldHeight; i+=CELLSIZE) {
            for (let j=0; j<fieldWidth; j+=CELLSIZE) {
                let cell = new PIXI.Sprite(
                    PIXI.loader.resources["img/background.jpg"].texture
                );

                cell.width = CELLSIZE;
                cell.height = CELLSIZE;
                cell.position.set(j, i);
                
                container.addChild(cell);
            }
        }

        return container;
    }

    function createPanel() {
        let x = 0.5*CELLSIZE;
        let y = app.renderer.height - 2.5*CELLSIZE;
        let width = app.renderer.width - CELLSIZE;
        let height = CELLSIZE*2;

        let roundBox = new PIXI.Graphics();
        roundBox.lineStyle(4, 0xB69479, 1);
        roundBox.beginFill(0xEAB67A);
        roundBox.drawRoundedRect(0, 0, width, height, 10)
        roundBox.endFill();                

        let dot = new createInteractFig("dot", CELLSIZE, roundBox);
        dot.x = roundBox.width/2 + CELLSIZE;
        dot.y = roundBox.height/2;

        let line = new createInteractFig("vline", CELLSIZE, roundBox);
        line.x = roundBox.width/2 - CELLSIZE;
        line.y = roundBox.height/2;

        let lineVertical = new createInteractFig("hline", CELLSIZE, roundBox);       

        let container = new PIXI.Container();

        container.addChild(roundBox);
        container.addChild(dot);
        container.addChild(line);
        container.addChild(lineVertical);

        container.x = x;
        container.y = y;
        container.zIndex = 500;

        return container;
    }

    function createInteractFig(type, CELLSIZE, roundBox) {
        console.log(onDragEnd);
        let fig;
        switch (type) {
            case "vline":
                fig = new createVertLine(CELLSIZE);
                break;
            case "hline":
                fig = new createHorisLine(CELLSIZE);
                break;

            case "dot":
                fig = new createDot(CELLSIZE);
                break;
            default:
                break;
        }
        fig.x = roundBox.width/2;
        fig.y = roundBox.height/2;

        fig.interactive = true;
        fig.buttonMode = true;
        fig.on('pointerdown', onDragStart)
            .on('pointerup', onDragEnd)
            .on('pointerupoutside', onDragEnd)
            .on('pointermove', onDragMove);
        fig.zIndex = 2000;

        return fig;
    }

    function onDragStart(event) {
        this.data = event.data;
        this.initialx = this.x;
        this.initialy = this.y;
        this.dragging = true;
    }


    function onDragEnd(event) {
        this.alpha = 1;
        this.dragging = false;
        checkPosition(this);

        //animation
        let d = new Dust(PIXI);
        function gameLoop() {
            requestAnimationFrame(gameLoop);
            d.update();
        }
        gameLoop();
        let starContainer = new PIXI.ParticleContainer(
            30,
            {alpha: true, scale: true, rotation: true, uvs: true}
        );
        app.stage.addChild(starContainer);
        let stars = d.create(
            this.data.global.x, this.data.global.y, 
            () => new PIXI.Sprite(
                PIXI.utils.TextureCache["img/background.png"]
            ), 
            starContainer,
            50
        );
    }

    
    function onDragMove() {
        if (this.dragging) {
            const newPosition = this.data.getLocalPosition(this.parent);
                    this.x = newPosition.x;
                    this.y = newPosition.y;
        }
    }

    function checkPosition(dragFig) {
        let vx, vy;
        
        let reachX;
        let reachY;
        let rightLimit = CELLSIZE/3;
        
        for (let i=0; i<coordsMass.length; i++) {
            let reachFig = coordsMass[i];

            reachX = reachFig.x*CELLSIZE;
            reachY = reachFig.y*CELLSIZE;

            vx = Math.abs(dragFig.data.global.x - reachX);
            vy = Math.abs(dragFig.data.global.y - reachY);
            if ((vx < rightLimit) && (vy < rightLimit) && (dragFig.figtype == reachFig.type) && (!reachFig.drawed)) {
                let drawFig = null;
                switch (reachFig.type) {
                    case "point": 
                        drawFig = new createDot(CELLSIZE);
                        drawFig.x = reachX;
                        drawFig.y = reachY;
                        reachFig.drawed = true;
                        break;

                    case "vline":
                        drawFig = new createVertLine(CELLSIZE);
                        drawFig.x = reachX;
                        drawFig.y = reachY;
                        reachFig.drawed = true;
                        console.log("Drawn!");
                        break;

                    case "hline":
                        drawFig = new createHorisLine(CELLSIZE);
                        drawFig.x = reachX;
                        drawFig.y = reachY;
                        reachFig.drawed = true;
                        console.log("Drawn!");
                        break;
                }

                if (drawFig) {
                    playspace.addChild(drawFig);
                    currentLevelProgress++;
                    console.log(currentLevelProgress, levelProgress)
                    checkEndOfTheLevel();
                }

            } else {
                dragFig.x = dragFig.initialx;
                dragFig.y = dragFig.initialy;
            }

        }
    }

    function checkEndOfTheLevel() {
        console.log("CHECK END: ", currentLevelProgress, levelProgress);
        if (currentLevelProgress == levelProgress) {
            changeLevel();
        }
    }
    
    function gameOverScene() {
        let gameOverScene = new PIXI.Container();
        
        let style = new PIXI.TextStyle({
            fontFamily: "Futura",
            fontSize: 64,
            fill: "white"
        });

        let background = new PIXI.Graphics();
        background.beginFill(0x000000);
        background.drawRect(0,0,app.renderer.width, app.renderer.height, 10);
        background.endFill();
        gameOverScene.addChild(background);

        let message = new PIXI.Text("The End!", style);
        message.pivot.set(message.width / 2 , message.height / 2)
        console.log(message.width)
        message.x = app.renderer.width/2;
        message.y = app.renderer.height/2;
        gameOverScene.addChild(message);     
        
        app.stage.addChild(gameOverScene);
    }

    function playScene(startGameContainer) {
        app.stage.removeChild(startGameContainer);
        setup();
    }

    function startGame(app, cellsize) {
        let container = new PIXI.Container();
        let background = new PIXI.Graphics();
        let buttonContainer = new PIXI.Container();
        let button = new PIXI.Graphics();
        let buttonText;

        background.beginFill(0xF1E0B1);
        background.drawRect(0,0,app.renderer.width, app.renderer.height);
        background.endFill();


        button.beginFill(0xB69479);
        button.drawRoundedRect(0,0, cellsize*3, cellsize, cellsize);
        button.pivot.set(button.width / 2 , button.height / 2);
        button.endFill();  
        button.interactive = true;
        button.buttonMode = true;
        button.on('pointerup', () => {
            playScene(container)
        });

        button.on('pointerover', () => {
            c.pulse(button, 30, 0.5);
            c.scale(
                button,         //The sprite
                1.2,         //The final x scale value
                1.2,         //The final y scale value
                30);   //The duration, in frames)
        });
        
        button.on('pointerout', () => {
            c.scale(
                button,         //The sprite
                1,         //The final x scale value
                1,         //The final y scale value
                60);   //The duration, in frames)
        })

        let c = new Charm(PIXI);

        function gameLoop(){

            requestAnimationFrame(gameLoop);
        
            c.update();
        }

        gameLoop();

        
        let style = new PIXI.TextStyle({
            fontFamily: "Arial",
            fontSize: 12,
            fill: "white"
        });

        buttonText = new PIXI.Text("START", style);
        buttonText.pivot.set(buttonText.width / 2 , buttonText.height / 2);

        buttonContainer.addChild(button);
        buttonContainer.addChild(buttonText);
        buttonContainer.x = app.renderer.width/2;
        buttonContainer.y = app.renderer.height/2;

        container.addChild(background);
        container.addChild(buttonContainer);

        app.stage.addChild(container);
    }
    
    document.body.appendChild(app.view);
}